# COLOR |SPACE|

## About

COLOR |SPACE| is a browser game designed to lend some insight into how the Discrete Cosine Transform works from a visual perspective. The game is contained entirely within the colorSpace.html file, and thus can be played simply by loading that file into a web browser. The game is also available to play for free at: https://sacrificialprawn.itch.io/color-space

Note that the game itself contains a tutorial on the specifics of how to play.

If you find any bugs or have any suggestions for improvement, feel free to raise an issue.

## License
This project is released under the MIT license.
